import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

import time
import pickle
import os
from myStorage import *


from sklearn.preprocessing import StandardScaler
from numpy import linalg as LA

from sklearn.model_selection import train_test_split

from sklearn.preprocessing import MinMaxScaler
from sklearn.feature_selection import VarianceThreshold

from sklearn import metrics
from sklearn.metrics import confusion_matrix
from sklearn.metrics import plot_confusion_matrix
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import accuracy_score

from sklearn import neighbors, datasets
from sklearn import tree as tree
from sklearn.tree import DecisionTreeClassifier, plot_tree
from sklearn.naive_bayes import GaussianNB
from sklearn.ensemble import RandomForestClassifier

from google.cloud import storage

def load_data():
    
    # 1. Load data
    print("Loading DATA....")
    
    dataw = pd.read_csv("./data/winequality-red2.csv")
    #dataw = pd.read_csv('winequality-red2.csv')
    data = dataw.copy()
    
    print("\n")
    print("Data Loaded information:")
    print("\n")
    data.info()
    pd.set_option('display.max_rows', None)
    n_data = data.shape[0]

    return data   

  
def split(data, predicteurs, target):
    
    # 2. Select predicteurs / target and split
    print("\n\n")
    print("Predicteurs selection and Split test and train set:")
    print("--------------------------------------------------:")
        
    target_column='quality'
    
    train, test = train_test_split(data, test_size = 0.4, stratify = data['quality'], random_state = 20)

    X_train = train[predictors]
    Y_train = train[[target_column]]

    X_test  = test[predictors]
    Y_test = test[[target_column]]

    
    # 2a. Report about train and test data (Optional)
    print("\n\n")
    print("Info train set:")
    print("--------------")
    X_train.info()
    Y_train.info()
    
    print("\n\n")
    print("Info test set:")
    print("--------------")
    X_test.info()
    Y_test.info()

    # 2b. Report about train and test data (Optional)
    print("\n\n")
    print("Info target set:")
    print("--------------")
    print(Y_train.groupby('quality').size())
    print(Y_test.groupby('quality').size())
    
    return X_train, X_test, Y_train, Y_test 
        

def varianceThresholding(X_train):

    # 3. Identifiez varibles a reenir pour thresholng variance
    print("\n\n")
    print("Predicteurs variance tresholding - Feature Engineering:")
    print("------------------------------------------------------")
    plt.figure(figsize = (20,8))
    scaler = MinMaxScaler()
    X_train_normalized = scaler.fit_transform(X_train)
    selector = VarianceThreshold()
    X_train_tresholding = selector.fit_transform(X_train_normalized)
    plt.bar(x=predictors, height=selector.variances_, color='orange')
    
    print("Predictor variances: \n")
    print(selector.variances_)


def myMetrics(Y_test, y_predicted_test, evalTime, modelName, dfMetrics, model):
    
    # 4. METRICS
    print("\n\n")
    print("Calculating metrics for model : ", modelName)

    accuracy= accuracy_score(y_predicted_test, Y_test)
    print("Accuracy: {}", accuracy.round(3))
    precision =  metrics.precision_score(Y_test, y_predicted_test, labels=None, pos_label=1, average='weighted', 
                                            sample_weight=None,zero_division='warn')
    recall =  metrics.recall_score(Y_test, y_predicted_test,  average='macro') 
    f1Score = metrics.f1_score(Y_test, y_predicted_test, average='macro')

    dfMetrics[modelName]['Precision'] = precision.round(3)
    dfMetrics[modelName]['Accuracy'] = accuracy.round(3)
    dfMetrics[modelName]['Recall'] = recall.round(3)
    dfMetrics[modelName]['F1Score'] = f1Score.round(3)
    dfMetrics[modelName]['FitTime'] = evalTime
    print("\n\n Metrics model: ", modelName)
    dfMetrics
    
    # Plot it
    disp = plot_confusion_matrix(model, X_test, Y_test, normalize='true',cmap=plt.cm.Blues)
    plt.savefig('src/reports/confusion_matrix_' + modelName + '.png')


    return dfMetrics
   
def bayes(X_train, Y_train, X_test, Y_test):
    print("\n")
    print("....Fitting Bayes Model...")

    startTime = time.time()
    gnb = GaussianNB()
    gnb.fit(X_train, Y_train)
    endtime = time.time()
    evalTime = endtime - startTime

    y_predicted_test= gnb.predict(X_test)
    y_predicted_test
    y_predicted_train = gnb.predict(X_train)
    y_predicted_train
    
    # Saving model to disk
    print("Creating pickle version: ")
    pickle.dump(gnb, open('src/models/model_Bayes.pkl','wb'))
    
    print("Finish Bayes Model...")
    return y_predicted_test, y_predicted_train, evalTime, gnb

def tree_Gini(X_train, Y_train, X_test, Y_test):
    print("\n")
    print("...Fitting tree_Gini Model")
    startTime = time.time()

    desicionTree = tree.DecisionTreeClassifier(max_depth =3, criterion='gini', random_state=20, min_samples_leaf=2)
    desicionTree.fit(X_train, Y_train)
    plt.figure(figsize = (10,8))
    plot_tree(desicionTree, feature_names = predictors, filled = True)

    y_predicted_test= desicionTree.predict(X_test)
    y_predicted_train = desicionTree.predict(X_train)
    endtime = time.time()
    evalTime = endtime - startTime
    
    # Saving model to disk
    print("Creating pickle version: ")
    pickle.dump(desicionTree, open('src/models/model_treeGini.pkl','wb'))

    print("Finish tree_Gini Model")
    return y_predicted_test, y_predicted_train, evalTime, desicionTree

def rForest(X_train, Y_train, X_test, Y_test):
    print("\n")
    print("Fitting rForest Model")
    
    startTime = time.time()
    clf = RandomForestClassifier(n_jobs=4, random_state=30)
    clf.fit(X_train, Y_train)

    y_predicted_test= clf.predict(X_test)
    y_predicted_train = clf.predict(X_train)
    
    endtime = time.time()
    evalTime = endtime - startTime

    # Saving model to disk
    print("Creating pickle version: ")
    pickle.dump(clf, open('src/models/model_rForest.pkl','wb'))
 
    print("Finish rForest Model")
    return y_predicted_test, y_predicted_train, evalTime, clf

def prediction_model (Xp):
    
    # Loading model to compare the results
    model = pickle.load(open('src/models/model_treeGini.pkl','rb'))
    print(model.predict(Xp))
    

if __name__ == '__main__':
    
    print("Executing models and calculating metrics")
    print("----------------------------------------")
    
    predictors =['fixed acidity','volatile acidity', 'citric acid','residual sugar','chlorides',
                  'free sulfur dioxide','total sulfur dioxide','density','pH','sulphates','alcohol']
    
    target="quality";
    
    models = ['Bayes', 'Tree_Gini', 'RForest']
    metrics_labels = ['Accuracy', 'Precision', 'Recall', 'F1Score', 'FitTime']

    print("main - Load Data")
    data = load_data()
    
    print("main - Split")
    X_train, X_test, Y_train, Y_test  = split(data, predictors, target )
    
    print("main - Thresholding")
    varianceThresholding(X_train)
    
    print("\n\n")
    print("main - Modelisation")
    print("----------------------------------------")
    dfMetrics = pd.DataFrame(index=metrics_labels, columns=models)
        
    y_predicted_test, y_predicted_train, evalTime, bayesModel = bayes(X_train, Y_train, X_test, Y_test)
    dfMetrics = myMetrics(Y_test, y_predicted_test, evalTime, "Bayes", dfMetrics, bayesModel)
    
    y_predicted_test, y_predicted_train, evalTime, treeGiniModel = tree_Gini(X_train, Y_train, X_test, Y_test)
    dfMetrics = myMetrics(Y_test, y_predicted_test, evalTime, "Tree_Gini", dfMetrics, treeGiniModel)

    y_predicted_test, y_predicted_train, evalTime, rForestModel = rForest(X_train, Y_train, X_test, Y_test)
    dfMetrics = myMetrics(Y_test, y_predicted_test, evalTime, "RForest", dfMetrics, rForestModel)
    print(dfMetrics)
    
    print("Saving Metrics Report")
    print("----------------------------------------")
    
    # SAVE MODEL AND METRICS IN GOOGLE CLOUD STORAGE - GCS 
    
    # Setup with GCS
    os.environ['GOOGLE_APPLICATION_CREDENTIALS'] = 'src/keys/iamodelpro-f5c180d6c629.json'
    bucket_name = 'dvc_bucket_mymodel'
    
    # SAVE METRICS - Files used by DVC 
    np.savetxt('src/reports/metrics.txt', dfMetrics.values, fmt='%s', delimiter="\t", header="Metrics\tTree_Gini\tBayes\tRForest")
    
    uploadFileToBucket('metrics.txt', 'src/metrics/metrics.txt', bucket_name)
    

    
    uploadFileToBucket('model_Bayes.pkl', 'src/models/model_Bayes.pkl', bucket_name)
    uploadFileToBucket('model_treeGini.pkl', 'src/models/model_treeGini.pkl', bucket_name)
    uploadFileToBucket('model_rForest.pkl', 'src/models/model_rForest.pkl', bucket_name)
    

