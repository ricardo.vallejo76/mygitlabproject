import os
from google.cloud import storage

def create_bucket(bucket_name):
    
    storage_client = storage.Client()
    bucket = storage_client.bucket(bucket_name)
    bucket.location = 'US'
    bucket = storage_client.create_bucket(bucket)
    vars(bucket)


def uploadFileToBucket(blob_name, file_path, bucket_name):
    
    try:
        storage_client = storage.Client()
        my_bucket = storage_client.get_bucket(bucket_name)
        blob = my_bucket.blob(blob_name)
        blob.upload_from_filename(file_path)
        return True
    except Exception as e:
        print(e)
        return
    