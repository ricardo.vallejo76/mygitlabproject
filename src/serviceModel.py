import pickle

def prediction_model (Xp):
    
    # Loading model to compare the results
    modelGini = pickle.load(open('src/models/model_treeGini.pkl','rb'))
    modelBayes = pickle.load(open('src/models/model_Bayes.pkl','rb'))
    modelForest = pickle.load(open('src/models/model_rForest.pkl','rb'))
    
    return modelGini.predict(Xp), modelBayes.predict(Xp), modelForest.predict(Xp)
    

if __name__ == '__main__':
    
    print("Executing models and calculating metrics")
    print("----------------------------------------")
    
    predictors =['fixed acidity','volatile acidity', 'citric acid','residual sugar','chlorides',
                  'free sulfur dioxide','total sulfur dioxide','density','pH','sulphates','alcohol']
    
    target="quality";
    
    models = ['Tree_Gini', 'Bayes', 'RForest']
    metrics_labels = ['Accuracy', 'Precision', 'Recall', 'F1Score', 'FitTime']


    
    # Predefined Predictions
    print ("\nPredefined test predictions: \n\n")
    print ("Xp=> [ 7.5, 0.27, 0.34, 2.3, 0.05, 4,8, 0.9951, 3.4, 0.64, 11 ]\n")
    
    # Xp=[[7.4, 0.7, 0, 1.9, 0.076, 11, 34, 0.9978, 3.51, 0.56, 9.4]]
    # Xp=[[11.2, 0.28, 0.56, 1.9, 0.075, 17, 60, 0.998, 3.16, 0.58, 9.8]]
    
    Xp=[[7.5,0.27,0.34,2.3,0.05,4,8,0.9951,3.4,0.64,11]]

    YpredGini, YpredBayes, YpredForest = prediction_model (Xp)
    
    print (" Desicion Tree - Gini Prediction Quality: " , YpredGini)
    print (" Bayes -  Prediction Quality: " , YpredGini)
    print (" Random Forest - Prediction Quality: " , YpredGini)
    
    # User defined Predictors
    print ("\nUser defined prediction: \n")
    
    print ("\n\nEnter the value of predictors: \n")
    fixed_acidity = input('Enter fixed acidity: ')
    volatile_acidity = input('Enter volatile acidity: ')
    citric_acid = input('Enter citric acidy: ')
    residual_sugar = input('Enter residual sugar: ')
    chlorides = input('Enter chlorides: ')
    free_sulfur_dioxide = input('Enter Free sulfur dioxide: ')
    total_sulfur_dioxide = input('Enter total sulfur dioxide: ')
    density = input('Enter density: ')
    pH = input('Enter pH: ')
    sulphates = input('Enter sulphates: ')
    alcohol = input('Enter alcohol: ')
    
    Xp = [[fixed_acidity, volatile_acidity, citric_acid, residual_sugar, chlorides, free_sulfur_dioxide, total_sulfur_dioxide, density, pH, sulphates, alcohol ]]
 
    YpredGini, YpredBayes, YpredForest = prediction_model (Xp)
    print ("\n")
    print (" Desicion Tree - Gini Prediction Quality: " , YpredGini)
    print (" Bayes -  Prediction Quality: " , YpredGini)
    print (" Random Forest - Prediction Quality: " , YpredGini)
    